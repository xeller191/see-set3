import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Contact from "./components/Contact";
import Header from "./components/Header";
import Product from "./components/Product";
import Usecase2 from './components/Usecase2';
export default function App() {
  return (
      <Router>
          <Switch>
            <Route path="/product">
              <Product />
            </Route>
            <Route path="/usecase">
              <Usecase2 />
            </Route>
            <Route path="/pricing">
              <Pricing />
            </Route>
            <Route path="/">
              <Contact />
            </Route>
          </Switch>
      </Router>
  );
}



function Pricing() {
  return <h2>About</h2>;
}



function Users() {
  return <h2>Users</h2>;
}