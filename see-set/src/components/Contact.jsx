import React, { useState } from "react";
import GoogleMapReact from "google-map-react";
import '../assets/scss/contact.scss'
import Header from '../components/Header'
import { Rotate as Hamburger, Rotate } from 'hamburger-react'
const Contact = () => {
  const [isOpen , setIsOpen] = useState(false)
  const props = {
    center: {
      lat: 13.895592228709159,
      lng: 100.65680773264573,
    },
    zoom: 16,
  };
  const renderMarkers = (map, maps) => {
    let marker = new maps.Marker({
      position: { lat: 13.895592228709159, lng: 100.65680773264573 },
      map,
      title: "Hello World!",
    });
    return marker;
  };
  return (
    <div>
        <div className="container__header">
            <Header />
        </div>
      <div className="googlemap">
      <div onClick={()=>setIsOpen(!isOpen)} className="menu">
        <Hamburger toggled={isOpen} />
      </div>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyAxPiV7f9KXOE9P49a8OSEjcFuZRRfGY7I" }}
          defaultCenter={props.center}
          defaultZoom={props.zoom}
          onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
        >
        </GoogleMapReact>
      </div>
      <div className="id2">
        <h1 >Zenalyse Company Limited</h1>
        <p>เวลาทำการ วันจันทร์ – วันศุกร์ 08:30 – 17:30 น.</p>
      </div>
      <div className="container__grid">
        <div className="section1">
          <h1>สำนักงาน</h1>
          <p>555/94 B-Avenue ถ.สุขาภิบาล5 แขวงออเงิน เขตสายไหม กรุงเทพมหานคร 10220</p>
        </div>
        <div className="section2">
          <h1>อีเมล</h1>
          <p>sales@zenalyse.co.th</p>
        </div>
        <div className="section3">
          <h1>ศูนย์บริการลูกค้า</h1>
          <p>02-1587338-9</p>
          <h1>แฟกซ์</h1>
          <p>02-1587340</p>
        </div>
        <div className="section4">
          <h1>ส่งข้อความหาเราทันที</h1>
        </div>
      </div>
    </div>
  );
};

export default Contact;

// clss component
// import React, { Component } from 'react';
// import GoogleMapReact from 'google-map-react';

// class Contact extends Component {
//   static defaultProps = {
//     center: {
//       lat: 13.895592228709159,
//       lng: 100.65680773264573
//     },
//     zoom: 16
//   };
//   // 13.895687114852013, 100.65679318669406
//   renderMarkers = (map, maps) => {
//     let marker = new maps.Marker({
//     position: { lat:13.895592228709159, lng: 100.65680773264573 },
//     map,
//     title: 'Hello World!'
//     });
//     return marker;
//    };
//   render() {
//     return (
//       <div style={{ height: '50vh', width: '100%' }}>
//         <GoogleMapReact
//           bootstrapURLKeys={{ key: "AIzaSyAxPiV7f9KXOE9P49a8OSEjcFuZRRfGY7I"}}
//           defaultCenter={this.props.center}
//           defaultZoom={this.props.zoom}
//           onGoogleApiLoaded={({ map, maps }) => this.renderMarkers(map, maps)}
//         >
//           {/* <AnyReactComponent
//             lat={13.895592228709159}
//             lng={100.6568077326457}
//             text="My Marker"
//           /> */}
//         </GoogleMapReact>
//       </div>
//     );
//   }
// }

// export default Contact;
