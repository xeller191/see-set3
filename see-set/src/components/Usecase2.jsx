import React from 'react'
// import '../assets/scss/_reset.scss'
import '../assets/scss/usecase2.scss'
import Header from './Header'
// import '../assets/css/usecase.css'

import SEESETGraphic from '../assets/img/usecase/SEESETGraphic.png'
import Move_TowerlightStatus from '../assets/img/usecase/Move_TowerlightStatus.gif'
import Move_IdleMachine from '../assets/img/usecase/Move_IdleMachine.gif'
import Move_Worker from '../assets/img/usecase/Move_Worker.gif'
import Move_LineMachine from '../assets/img/usecase/Move_LineMachine.gif'
import Move_Order from '../assets/img/usecase/Move_Order.gif'
import Move_Repair_Test from '../assets/img/usecase/Move_Repair_Test.gif'
import Move_Think_test from '../assets/img/usecase/Move_Think_test.gif'
const Usecase2 = () => {
    return (
        <div className="main">
            <div id="container__header">
                <Header />
            </div>
            <div className="section1__container">
                <div>
                    <div className="item1" >
                    </div>
                    <div className="item2">
                        <h1>ผลการสนับสนุนการพัฒนา</h1>
                        <p>โซลูชั่นช่วยคุณเปลี่ยนแปลงโรงงานอุตสาหกรรมให้เข้าสู่ยุค Industry 4.0 ให้การเก็บข้อมูลจากเครื่องจักรเป็นเรื่องง่ายๆในรูปแบบข้อมูล Digital พร้อมสำหรับการวิเคราะห์ตามความต้องการต่างๆ ทั้งหาสาเหตุของปัญหาที่เกิดจากการผลิต ความต้องการลดความสูญเสีย ทำกิจกรรม Kaizen และ Lean production manufacturing</p>
                    </div>
                </div>
            </div>

            {/* note support meduim and learg screen */}
            <div className="section2__container">
                <div className="bg1">
                </div>
                <div>
                    <div className="item3">
                        <span><h1>ภาพทั้งโรงงานจะอยู่บนอุปกรณ์ในมือคุณ</h1></span>
                        <p>โรงงานที่มีขนาดกว้างใหญ่การตรวจสอบสถานะการทำงานของเครื่องจักร เป็นเรื่องที่เป็นไปได้ยากและใช้เวลา แต่ SEESET จะช่วยให้คุณมองเห็นทั้งโรงงานของคุณในหน้าจอเดียว สามารถเปิดบนอุปกรณ์มากมายผ่าน web browser</p>
                    </div>
                    <div className="item4">
                        <img src={SEESETGraphic}/>
                    </div>
                </div>
            </div>

            <div className="section3__container">
                <div>
                    <div className="item5">
                        <img src={Move_TowerlightStatus} alt=""/>
                    </div>
                    <div className="item6">
                        <span><h1>ภาพทั้งโรงงานจะอยู่บนอุปกรณ์ในมือคุณ</h1></span>
                        <p>โรงงานที่มีขนาดกว้างใหญ่การตรวจสอบสถานะการทำงานของเครื่องจักร เป็นเรื่องที่เป็นไปได้ยากและใช้เวลา แต่ SEESET จะช่วยให้คุณมองเห็นทั้งโรงงานของคุณในหน้าจอเดียว สามารถเปิดบนอุปกรณ์มากมายผ่าน web browser</p>
                    </div>
                </div>
            </div>

            <div className="section4__container">
                <div>
                    <div className="item7">
                        <div>

                        </div>
                        {/* <img src={Move_IdleMachine} alt=""/> */}
                    </div>
                    <div className="item8">
                        <span><h1>ภาพทั้งโรงงานจะอยู่บนอุปกรณ์ในมือคุณ</h1></span>
                        <p>โรงงานที่มีขนาดกว้างใหญ่การตรวจสอบสถานะการทำงานของเครื่องจักร เป็นเรื่องที่เป็นไปได้ยากและใช้เวลา แต่ SEESET จะช่วยให้คุณมองเห็นทั้งโรงงานของคุณในหน้าจอเดียว สามารถเปิดบนอุปกรณ์มากมายผ่าน web browser</p>
                    </div>
                </div>
            </div>

            <div className="section5__container">
                <div>
                    <div className="item9">
                        <div>

                        </div>
                    </div>
                    <div className="item10">
                        <span><h1>Line Status</h1></span>
                        <p>สายงานการผลิตรูปแบบ Assembly line มีการผลิตหลายขั้นตอน การติดตั้งระบบ SEESET ทำให้หัวหน้างานหรือฝ่ายซ่อมบำรุง สามารถรู้จุดที่เกิดความขัดข้องหรือผิดพลาดได้ในทันที ซึ่งช่วยลดระยะเวลาในการซ่อมแซม นอกจากนี้เวลาที่เกิดเหตุขัดข้อง SEESET จะบันทึกเวลาที่เกิดเหตุการณ์นั้นๆ จนกระทั่งเหตุการณ์นั้นสิ้นสุดลง พร้อมยังบันทึกสาเหตุของการเกิดเหตุการณ์ดังกล่าวได้ เพื่อนำไปวิเคราะห์ ป้องกันและแก้ไข ไม่ให้เกิดปัญหานั้นซ้ำอีก</p>
                    </div>
                </div>
            </div>

            <div className="section6__container">
                <div>
                    <div className="item11">
                        <img src={Move_Worker} alt=""/>
                    </div>
                    <div className="item12">
                        <span><h1>การปฏิบัติงานของพนักงาน</h1></span>
                        <p>คุณจะรู้ได้อย่างไรว่า พนักงานปฏิบัติงานได้เต็มเวลา และเต็มประสิทธิภาพ มีกรณีศึกษา 1 กรณี เครื่องจักรมี run rate 100% แต่ไม่มีผลผลิตออกมา หรือออกมาน้อย เมื่อสอบถามพนักงานแล้ว พนักงานแจ้งว่า เครื่องจักรผลิตช้า มักจะพบสาเหตุดังกล่าวในกะการทำงานช่วงกลางคืน เมื่อติดตั้งระบบ SEESET แล้ว ปรากฏว่า พนักงานปล่อยให้เครื่อง Run ไปเฉยๆ โดยไม่เติมวัตถุดิบ กักตุนจำนวนการผลิตไว้ เพื่อจะได้เอาไว้ทำในช่วง O.T. ในวันต่อๆไป</p>
                    </div>
                </div>
            </div>

            <div className="section7__container">
                <div>
                    <div className="item13">
                        <img src={Move_Order} alt=""/>
                    </div>
                    <div className="item14">
                        <span><h1>มีออเดอร์จำนวนมาก ผลิตไม่ทัน</h1></span>
                        <p>พนักงานทำเรื่องขอซื้อเครื่องจักรเพิ่ม เนื่องจากผลิตไม่ทันตาม Order ทางผู้บริหารมีการวิเคราะห์ถึงสาเหตุที่แท้จริง โดยการ monitor การทำงานของเครื่องจักร พบว่า เครื่องจักรมีเหตุการณ์ที่ทำให้หยุดผลิตบ่อยครั้ง จึงทำการติดตั้งระบบ SEESET เพื่อเก็บข้อมูล พบว่าเครื่องจักรเกิด Alarm สูงมาก เนื่องจาก Mold หรือแม่พิมพ์มีปัญหา จากที่จะต้องลงทุนซื้อเครื่องจักรใหม่มูลค่าหลายสิบล้าน นำข้อมูลที่ได้ มาแก้ไข Mold ทำให้ประสิทธิภาพการทำงานของเครื่องจักรเป็นไปอย่างต่อเนื่อง สามารถสร้างผลผลิตได้จำนวนมากขึ้น ในระยะเวลาเท่าเดิม</p>
                    </div>
                </div>
            </div>

            <div className="section8__container">
                <div>
                    <div className="item15">
                        <img src={Move_Repair_Test} alt=""/>
                    </div>
                    <div className="item16">
                        <span><h1>ซ่อมแซมเครื่องจักรได้ทันที</h1></span>
                        <p>รู้ได้ทันทีเมื่อเครื่องจักรเกิดปัญหาขึ้น ทำให้สามารถเข้าไปแก้ปัญหาได้อย่างรวดเร็ว ลดเวลาการแจ้งปัญหามายัง Maintanace เพื่อให้การผลิตดำเนินการต่อได้ตรงตามแผนการผลิตที่ถูกวางเอาไว้</p>
                    </div>
                </div>
            </div>

            <div className="section9__container">
                <div>
                    <div className="item17">
                        <img src={Move_Think_test} alt=""/>
                    </div>
                    <div className="item18">
                        <span><h1>เข้าใจแผนการผลิตของวันอย่างรวดเร็ว</h1></span>
                        <p>แสดงแผนการผลิตแบบทั้งวัน ซึ่งเหมาะกับผู้รับผิดชอบหน้าเครื่องจักร วันนี้เครื่องไหนต้องผลิตอะไรบ้าง และเป็นไปตามแผนการผลิตหรือไม่</p>
                    </div>
                </div>
            </div>


            <div className="section10__container">
                <h1>
                    สนใจตัวช่วยในการพัฒนาติดต่อเรา
                </h1>
                <p>
                    คุณจะรู้ได้อย่างไรว่า ข้อมูลที่คุณนำมาพัฒนานั้นถูกต้องและน่าเชื่อถือเพียงพอ 
                    เราคือผู้พัฒนาซอร์ฟแวร์ที่จะช่วยให้คุณสามารถรู้ทุกความเคลื่อนไหวในการผลิตได้แบบ Real-Time อีกทั้งยังบันทึกข้อมูลเหล่านั้นมาสรุปให้คุณสามารถคำไปวิเคราะห์ต่าง ๆ ได้อย่างมากมาย
                </p>
                <div >
                    <h2>
                        ติดต่อเรา
                    </h2>
                </div>
            </div>
        </div>
    )
}

export default Usecase2
